y = 4;

nosnik_x = 180;
nosnik_y = 2;
nosnik_z = 30;
nosnik_z1 = 18;

noha_x = 80;
noha_y = 45;

module nutM6() {
   difference() {
       cube([10+2+2, 10+2+2, 6]);
       translate([1.5, 1.5, 1])
           cube([11, 11, 7]);
       translate([7, 7, -1])
       cylinder(d=6.25, h=3, $fn=16);
   }
}

module nutM6z(x_pos=5, z_pos) {
    translate([x_pos, 0,  14 + z_pos])
        rotate([-90, 0, 0])
            nutM6();
}

module nutHole(x_pos=5, z_pos) {
    translate([x_pos, -1, z_pos])
        cube([14, 7.1, 14]);
}

module sideWall(rotate=90, c=0) {
    yy = y+2;
    xx = noha_y + y + yy;
    h = 14;
    translate([-yy*c, xx*c, 0]) 
        rotate([0, 0, rotate]) {
            difference() {
                cube([xx, yy, nosnik_z1]);
                translate([xx/2 - h/2, -0.5, nosnik_z1/2 - h/2])
                    cube([h, yy+1, h]);
            }
            translate([xx/2 - h/2, 6,  nosnik_z1/2 - h/2])
                rotate([90, 0, 0])
                    nutM6();
        }
}


translate([nosnik_x/2 - noha_x/2, 0, 0])
    sideWall();

translate([nosnik_x/2 + noha_x/2 + 6, 0, 0])
    sideWall(rotate=-90, c=1);

difference() {
    difference() {
        union() {
            cube([nosnik_x, y , nosnik_z]);
            translate([0, y, 0]) {        
                cube([nosnik_x/2 - noha_x/2, nosnik_y, nosnik_z]);
                translate([nosnik_x/2 + noha_x/2 , 0, 0])
                cube([nosnik_x/2 - noha_x/2, nosnik_y, nosnik_z]);
            }
        }
        
        nutHole(z_pos=0.5);
        nutHole(z_pos=nosnik_z - 0.5 - 14);
        nutHole(x_pos=nosnik_x - 5 - 14, z_pos=0.5);
        nutHole(x_pos=nosnik_x - 5 - 14, z_pos=nosnik_z - 0.5 - 14);
    }
    nosnik_x1 = nosnik_x - 2*(2*5 + 14);
    translate([(nosnik_x - nosnik_x1)/2, -0.5, nosnik_z1])
        cube([nosnik_x1, y+2+1, nosnik_z - nosnik_z1 + 1]);
}

nutM6z(z_pos=0.5);
nutM6z(z_pos=nosnik_z - 0.5 - 14);
nutM6z(x_pos=nosnik_x - 5 - 14, z_pos=0.5);
nutM6z(x_pos=nosnik_x - 5 - 14, z_pos=nosnik_z - 0.5 - 14);


translate([nosnik_x/2 - noha_x/2 - y - 2, noha_y + y, 0]) {
    difference() {
        cube([noha_x + 2*(y+2), y+2, nosnik_z1]);
        nutHole(noha_x/2 + y + 2 - 7, nosnik_z1/2 - 14/2);
    }
}

translate([nosnik_x/2 - noha_x/2 - y, noha_y + y, 0])    
    translate([noha_x/2 + y - 7, y+2,  nosnik_z1/2 - 14/2])
        rotate([90, 0, 0])
            nutM6();        
