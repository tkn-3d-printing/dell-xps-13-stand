Dell XPS 13 vertical stand
==========================

This project is a 3D model of a vertical stand for Dell XPS 13 laptop. The stand
itself must be mounted (hanged) on a Dell U2412M monitor stand or similar
(rectangular shape of dimensions approx. 80x40 mm).

The stand consists of three parts that must be assembled together:

Base (1 pc.)
    It connects all parts of the stand together and hangs on monitor stand.

.. figure:: base.png    

Hooks (2 pcs.)
    They hold the notebook in place actually.

.. figure:: hook.png

.. _OpenSCAD: http://www.openscad.org/

Model is designed using OpenSCAD_.

License
-------

.. _`Creative Commons Attribution-ShareAlike 4.0 International License`: http://creativecommons.org/licenses/by-sa/4.0/

.. image:: https://i.creativecommons.org/l/by-sa/4.0/88x31.png
   :alt: Creative Commons License
   :align: left

This work is licensed under the `Creative Commons Attribution-ShareAlike 4.0
International License`_.To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative
Commons, PO Box 1866, Mountain View, CA 94042, USA.

.. vim: ts=4 expandtab spelllang=en spell textwidth=80 fo-=l: 
