
y = 6;
drzak_x = 133+y+10+y;
drzak_x1 = 6 + 66;
z = 15;

d = 5.25;
r1 = 10.25/2;
r2 = d/2;
h = 3.1;

ntb_y = 22.5;

module hole(x_pos, z_pos) {
    translate([x_pos, 0, z_pos]) {
        translate([0, -1, - d/2])
            translate([0, 0, d/2])
                rotate([-90, 0, 0]) 
                    cylinder(d=d, h=y+2, $fn=16);                
                
        translate([0, 0, 0])
            translate([0, y+0.1, 0]) 
                rotate([90, 0, 0])
                    cylinder(r1=r1, r2=r2, h=h, $fn=16);
    }
}

module hook() {
    difference() {
        union() {
            cube([drzak_x, y, z]);
        }
        hole(23, 0.5+14/2);
        hole(23 + 14 + 1, 0.5+14/2);
    }

    translate([drzak_x - y, 0, 0]) {
        cube([y, ntb_y + 2*y, z]);
    }

    translate([drzak_x - drzak_x1, y + ntb_y, 0]) {
        cube([drzak_x1, y, z]);
    }
}

hook();

translate([drzak_x, 2*ntb_y + 3*y, 0])
    rotate([0, 0, 180])
        hook();

